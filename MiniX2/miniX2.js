let button;
let state = true;

function setup() {
  createCanvas(600,700);
  background(250,250,250);
  button = createButton('Like this post');
  button.position(250, 420);
  button.mousePressed(buttonPressed);

}

function draw(){
  if(state === true) {
    makeHappyEmoji();
  } else {
    makeUnhappyEmoji();
  }
}


function buttonPressed(){
  if(state === true) {
    state = false;
  } else {
    state = true;
  }
}

function makeHappyEmoji() {
  background(250,250,250);
  scale(4);
  fill(246,212,100);
  noStroke();
  ellipse(75,75,50,50);
  //eyes
  fill(255,255,255);
  noStroke();
  ellipse(66,70,14,12);
  ellipse(84,70,14,12);
  fill(51,26,0);
  ellipse(66,72,8,7);
  ellipse(84,72,8,7);
  //cheeks
  fill(243, 122, 150);
  noStroke();
  ellipse(62,78,13,10);
  ellipse(88,78,13,10);
  //mouth
  noFill();
  stroke(155,64,64);
  arc(75, 75.8, 25, 30, QUARTER_PI,PI-QUARTER_PI);
}

function makeUnhappyEmoji() {
  scale(4);
  fill(0, 255,43);
  noStroke();
  rectMode(CENTER);
  rect(75,75,50,50);
  //eyes
  stroke(235,44,255);
  line(80,67,90,77);
  line(90,67,80,75);
  noStroke();
  fill(168,33,55);
  ellipse(66,72,8,8);
  //mouth
  stroke(235,44,255);
  noFill();
  arc(75, 90, 27, 26, PI,0);
}
