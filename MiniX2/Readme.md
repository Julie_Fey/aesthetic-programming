# **MiniX 2 - class 2**

[A Link To My Project](https://julie_fey.gitlab.io/aesthetic-programming/MiniX2/)

[My Code](https://gitlab.com/Julie_Fey/aesthetic-programming/-/blob/master/MiniX2/miniX2.js)

<img src="MiniX2.gif" width="500">


## **My project**
For my miniX2, i have designed two emojis, and made a button that switches between them. The first emoji that you see is a happy one, and it is based on the existing emoji that looks like this
<img src="smilingRosyCheeksEmoji.png" width="50">

Underneath there is a button which says "like this post", and when you click it, the happy emoji disapears and another one shows up. The new emoji is made of a rectangle and has one eye and it generally looks pretty sad. If you click the button again, the happy emoji will reappear. 




## **The technical aspect**

For this program i wanted to change bewteen two emojis, so i had to use a couple of if statements, which I had some help with figuring out. I started out defining a variable called state as true, which would help me switch between two states, which would display either the happy emoji or the unhappy emoji. In my draw function i made an if statement, which stated that if the state is true, then it should draw the happy emoji, and if it is false, it should draw the unhappy emoji. The difficult part was creating the button, which i had some help doing. When the button is pressed, it has an if statement, that says if the state is true, then make it false and vice versa. This means that when i press the button, it will alternate between drawing the happy emoji and the unhappy emoji. 
The emojis are made using a function instead of draw, which was something that i was advised to do. I believe it is because it then runs the code when called on, instead of drawing it in a loop. I made the emojis using ellipses, rect, line and arc. I looked at some code i found [here](https://editor.p5js.org/YanlinMa/sketches/SJafwAOn) to create the emojis, and modified it to look how i wanted. 




## **The idea with my program and what it means/represents**

I wanted to create two emojis that told some sort of story. I was inspired by what someone said during the lecture about how a circle excludes everything on the outside. That made me think of how we use emojis to express ourselves, but how we carefully select what we choose to show. Emojis can only show a generic form of what we are feeling, and it can never capture the essense of our emotions and expressions. Furthermore, it made me think of how we are carefully curating a certain narrative of ourselves online and on social media etc. So i wanted to show that everytime we use an emoji, we choose what we want people to see and therefore we hide some other part of ourselves. In my program, the happy emoji represents what we show to the world. It's like when people ask you how you are doing, and you just reply "fine" even if it is not true. That is what the happy emoji represents, the narrative we choose to let people see, all the unpersonal stuff we put on as a front for the world to see. This is also why i chose to base the happy emoji on an already existing one, to show how "picture perfect" and "safe" we often portray ourselves. The button is also a reference to social media, as we only ever post about the happy and good parts of our lives. But when the button is pressed, we see a different reality, a part of us that also exists, which is the unhappy emoji. Maybe it exists on a different day than the happy emoji, maybe they co-exist or maybe it is just hidden, but the point is that there is always something else to the person writing the emoji than what the emoji is able to express. 


