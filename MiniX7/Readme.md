# **Bubble-Breach!** #

[Link to my game!](https://julie_fey.gitlab.io/aesthetic-programming/MiniX7/)

[My code](https://gitlab.com/Julie_Fey/aesthetic-programming/-/blob/master/MiniX7/minix7.js)

[My class](https://gitlab.com/Julie_Fey/aesthetic-programming/-/blob/master/MiniX7/class.js)


<img src="MiniX7.gif" width="1000">

<img src="you_win.PNG" width="500">

<img src="you_lose.PNG" width="500">



## **My game - welcom to Bubble-Breach!** ##
My game and project for this week is called Bubble-Breach. The concept is that you have to break the green bubbles by going over them with the mouse, while trying to avoid the red bubbles. Whenever you breach a green bubble, you will gain one point, as displayed in the top left corner. When you breach a red bubble, you will lose one point. The game is won by reaching 25 points, and lost by reaching -25 points. The player will know the result of the game when the screen will display either "you win!" or "you lose" upon reaching either 25 or -25 points. 

The objects in the game are the bubbles. The bubbles are separated into two different classes, one for the red bubbles and one for the green. The classes are completely identical besides from the names, as all of their differences is defined in the main code instead of the class. The bubbles each have a number of attributes/methods; display, update, burst and delete. Display draws the bubbles, and update makes them move slightly. Burst makes sure that the bubbles dissapear when the mouse runs over them, and deletes removes them entirely from the game. When the player sees the bubbles dissapear, it is the Burst function. When you either win or lose the game, that's when the delete function comes in, as I didn't want them to be active behind the win/lose message. 

The concept of the game is inspired by [an example](https://editor.p5js.org/aferriss/sketches/H1XkWMRNX) i found online. The source code shows a single class of bubbles that you can burst, while a counter goes up until there is no more bubbles on the screen. I wanted to turn this into a game, and reused most of the code in the final product. 

Since i decided to use bubbles as my object, i avoided any too complex abstraction. The was not necessarily my intention, more so just something that accidentially came to be as i was trying to figure out what to make. It is not very controversial to claim that bubbles can be created and burst and not much more than that. However, if i had chosen a different kind of object, more consideration about the object's attributes would be needed. 
It is important to consider why you make the choices you do when working with objects. Most of us has a lot of bias and we more often than not think of other people or things as stereotypes. While stereotypes of course can be true, it is important not to base things upon them, since doing so enables someone to decide or dictate what someone else is or isn't. It can be extremely limiting, and it is important that we do not tell or decide anybody elses story. 

