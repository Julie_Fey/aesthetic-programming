
class Bubble { //winner bubble class
  constructor(tempX = 200, tempY = 200, tempSize = 50, tempColor = 0) {
    this.x = tempX;
    this.y = tempY;
    this.size = tempSize;
    this.color = tempColor;
    this.alive = true;
  }

  display() {
    if (this.alive) {
      noFill();
      strokeWeight(3);
      stroke(this.color);
      ellipse(this.x, this.y, this.size);
    }
  }

  update() {
    this.x = this.x + random(-1, 1);
    this.y = this.y + random(-1, 1);
  }

  burst() {
    let d = dist(mouseX, mouseY, this.x, this.y);
    if (d < this.size / 2) {
      // over the bubble
      this.alive = false;
      this.x = 10000;
      this.y = 10000;
      score++;
    }
  }

  delete() {
    for(let i =0; i < bubs.length; i++){
      bubs[i].splice();
    }
  }
}


class Bubble2 { // loser bubble class
  constructor(tempX = 200, tempY = 200, tempSize = 50, tempColor = 0) {
    this.x = tempX;
    this.y = tempY;
    this.size = tempSize;
    this.color = tempColor;
    this.alive = true;
  }

  display() {
    if (this.alive) {
      noFill();
      strokeWeight(3);
      stroke(this.color);
      ellipse(this.x, this.y, this.size);
    }
  }

  update() {
    this.x = this.x + random(-1, 1);
    this.y = this.y + random(-1, 1);
  }

  burst() {
    let d = dist(mouseX, mouseY, this.x, this.y);
    if (d < this.size / 2) {
      // over the bubble
      this.alive = false;
      this.x = 10000;
      this.y = 10000;
      score--;
    }
  }

  delete() {
    for(let i =0; i < bubs2.length; i++){
      bubs2[i].splice();
    }
  }
}
