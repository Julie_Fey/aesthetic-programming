let myFont;
let bubs = []; //empty array for the winner bubbles
let bubs2 = []; //empty array for the loser bubbles
let score = 0;

function preload() {
  myFont = loadFont('hippocket.ttf');
}


function setup() {
  createCanvas(windowWidth, windowHeight);
    textFont(myFont); // makes all of the text look delicious

  //all of the variabeles are devided in number one and number two. The first ones belong to the winner bubbles, and the two's belong to the loser bubbles
  for (let i = 0; i < 100; i++) {
    let x = random(width);
    let y = random(height);
    let x2 = random(width);
    let y2 = random(height);
    let sz = random(10, 100);
    let col = color("#A1CE9F");
    let col2 = color("#CE9FA7");

    let b = new Bubble(x, y, sz, col); // make new winner bubble
    bubs.push(b); // add it to the winner array

    let b2 = new Bubble2(x2, y2, sz, col2); // make new loser bubble
    bubs2.push(b2); // add it to the loser array
  }
}


function draw() {
  background("#F7F0CB");
  textSize(40);
  stroke("black");
  strokeWeight(2);
  fill("white");
  text(score, 50,50);

  for(let i =0; i < bubs.length; i++){ //calls the winner bubles
   	bubs[i].display();
    bubs[i].update();
    bubs[i].burst();
  }

  for(let i =0; i < bubs2.length; i++){ //calls the loser bubbles
    bubs2[i].display();
    bubs2[i].update();
    bubs2[i].burst();
  }

  scoreCount();
}


function scoreCount() { //decides when the game is won or lost
  if (score === 25) {
    displayWin();
  } if (score === -25) {
    displayLose();
  }
}


function displayWin() { //what happens when you win
background("#F7F0CB");
textSize(40);
noStroke();
textAlign(CENTER);
fill("#A1CE9F")
text("you win!", width/2, height/2);
noBubbles();
}


function displayLose() { //what happens when you lose
  background("#F7F0CB");
  textSize(40);
  textAlign(CENTER);
  noStroke();
  fill("#CE9FA7")
  text("you lose", width/2, height/2);
  noBubbles();
}


function noBubbles() { //deletes the bubbles when you have reached either the winner screen or loser screen. Otherwise it keeps bursting bubbles behind the backgroun, making the score keep changing
  bubs2[i].delete();
  bubs[i].delete();
}
