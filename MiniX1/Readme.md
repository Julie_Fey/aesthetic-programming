# MiniX 1 - Class 1

URL: https://julie_fey.gitlab.io/aesthetic-programming/MiniX1/
code: https://gitlab.com/Julie_Fey/aesthetic-programming/-/blob/master/MiniX1/sketch.js


Here is a gif of what i have made:
 <br>

<img src="MiniX_1.gif" width="600">

<br>

I have fooled around in particular with color, which i found a bit challenging, since i was unsure of how it worked theoretically. My end result displays a background which is constantly changing color, so that it looks as if it moves through all of the colors of the rainbow. In the middle of my screen i have played around with a new type of shape. It reminded me of PAC-man, so i made an ellipse for his eye and another one that he could eat. 

The process of making this started because i wanted to fill an ellipse with a rainbow color. This lead me to a google search result of someone writing some code in p5.js so that they could draw on their screen with a rainbow stroke. This is the link: https://kellylougheed.medium.com/rainbow-paintbrush-in-p5-js-e452d5540b25 
I tried to replicate it (which was a lot of fun), and then i wanted to see of i could reuse that code for something else. I couldn't figure out how to fill the ellipse with rainbow, but i figured out how to set the rainbow as the background. Furthermore, i struggled with having multiple geometrical shapes, because i wanted them to have different colors, and i could not get that to work. Instead i figured out how to make them transparent, so that i was able to see the ellipse (which is PAC-mans eye), on top of PAC-Man. It was really fun trying to create something indepentently, and even though it is very simple, i quite like my result. It was interesting to take a piece of code that i was unfamiliar with, and try to make sense of it by changing different parts, and then reapply them to something new. 

i see a lot of similarities with coding and writing, especially how it feels when you are creating something. What is really different though, is the approach. With reading or writing it feels very much as something that goes from A to B, but with code it feels like you are always int the middle of everyhting all at once. It has a puzzle-solving feel to it that reading or writing doesn't, allthough that might just be because i am so fluent in reading and writing now, that the puzzle aspect is lost on me. 

To me, code and programming means creativity and the ability to create. I am a huge fan of creating (anything and everyhting), and i find it hugely satisfying to be able to control, create and modify something that i can see in front of me. And i think, more than anything, that understanding is the coolest aspect of coding. It's like the feeling you have when you finally understood a math problem, that's the feeling i get when i understand some code, and it is extremely rewarding and cool. The texts we read are really helpful so far. They put a lot of things into perspective, and it inspires me to explore and think about the ways i can do something or change something through programming (in the future when i am better at it). It just makes me want to learn even more. 
