let state = true;

function setup() {
  createCanvas(windowWidth,windowHeight);
  frameRate(2);
  print("this throbber is just to distract you. while you look at it, i will steal your data and sell it to bill gates, and he will use it to plant a chip in you while you sleep, so that he can control your every move by putting you into the matrix");
  print("muahahahahah");
}

function draw() {
  background(239, 230, 174);
  noStroke();
  translate(width/2 - 200,height/2 - 200);
  grid();

  if (state === true) {
    moveRect();
    state = false;
  } else {
    moveEllipse();
    state = true;
  }
}

function grid(){
//the ellipses
  for (let x = 0; x <= 400; x = x + 90) {
    for (let y = 0; y <= 400; y = y + 90){
      fill(114,192,216);
      ellipse(x,y,25);
    }
  }

//the squares
  for (let rx = 40; rx <= 350; rx = rx + 90) {
    for (let ry = 40; ry <= 350; ry = ry + 90){
      fill(114, 216, 165);
      rectMode(CENTER);
      rect(rx, ry, 25);
    }
  }
}

function moveRect(){
//the moving square
  let move1 = [40, 130, 220, 310];
    fill(216, 114, 199);
    rectMode(CENTER);
    rect(random(move1), random(move1), 25);
}

function moveEllipse() {
//the moving ellipse
  let move2 = [0, 90, 180, 270];
    fill(216, 114, 199);
    ellipse(random(move2), random(move2), 25);
}
