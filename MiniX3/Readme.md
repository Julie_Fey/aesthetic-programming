[A Link To My Project](https://julie_fey.gitlab.io/aesthetic-programming/MiniX3/)

[My Code](https://gitlab.com/Julie_Fey/aesthetic-programming/-/blob/master/MiniX3/index.html)

<img src="Correct_MiniX3.gif" width="1000">


# **So... What did i make?** #
My throbber was initially inspired by the Playstation loading icon. 

<img src="playstation-loading-animation.gif" width="250">

(this was the only image i could find, the one i'm familiar with are just white strokes on a blue background, but the rest is the same).

I was initially inspired to create a throbber that used some sort of a geometric pattern, in which a part of it moved in a seemingly random way. The result is a throbber that consists of two grids, one of ellipses and one of squares. Together they form a square in the middle of the screen. Additionally we see one of the squares or ellipses being highlighted at all times. The highlighted figure changes at random and alternates between the ellipse and the square. 

### **The technical aspect** ###

Going into this mini exercise, I really wanted to try to use all of the different functions and elements we have learned about so far, to really prove to myself that i understand them and can use them in at least a simple way. For this reason i also didn't look at any code for inspiration, but wrote it only from memory and using the p5 refference page. The result consists of a number of things; i used for loops to create the grid, arrays to make the highligted square/ellipse, the translate function, multiple if-statements and of course the basis elements such as rectMode, frameRate and the random function. I also printed a little joke in the console log ;;))

### **Reflection on my project** ###

When deciding what to make, i thought about how we percieve time and in that regard, how we experience waiting for something, as it is here we often see a throbber. Waiting can often feel like a limbo, a feeling of not being able to do anything but stay in your current state until the thing you are waiting for has happened. For this reason i wanted to show the purposelessness by making the movement feel random and directionless. It crossed my mind to highlight the squares in a pattern, for example from one side to the other, but i really wanted that frustrating, limbo waiting feeling to come across. And that is why it moves the way it does. It gives you something to focus on, but also does not indicate how far in the waiting proces you are. This i did on purpose, again to create the right feeling of waiting numbness, instead of a feeling of anticipation, since i often feel like i am in limbo and unable to do anything but wait when i am waiting for something. So my project shows that time is passing, while also creating a limbo state where time suddenly doesn't seem to mean a linear path from A to B. 

### **Reflection on throbbers** ###

A throbbers function is to convey the message that you need to wait because something is loading. But after our instructor class i became aware that it is also a way for the developers to hide things from the user. So, it's a distraction. It either serves the purpose of creating a better user expereince, or it tries to hide what is going on for the user. For this reason, i decided to put a message in the console log. This is to show how the throbber can distract from what is really going on. I must admit that i don't really like the idea of a throbber very much. At best it is something that enhances the user experience by decieving the users. At worst, it decieves the users to exploit the users. Personally, i think that software should be as transparent as possible. Too much of the world aim to use you for their personal profit, and i think that not disclosing what is going on in software and code, is something that only helps people exploit innocent users. There is no reason why software cannot be transparent - we just need to make it the norm. It relates a bit to what Anette Vee says about coding as literacy - if people understood more of how software and code worked, then there would be less of a need to currate the user experience (for example by adding throbbers so people don't get confused and/or impatient), and with that we could avoid some of the countless people and companies who take advantage of peoples ignorance. Transparency creates understanding and people who understand will not be manipulated with.
