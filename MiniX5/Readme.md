# **MiniX5** #


[The project](https://julie_fey.gitlab.io/aesthetic-programming/MiniX5/)

[The code](https://gitlab.com/Julie_Fey/aesthetic-programming/-/blob/master/MiniX5/miniX5.js)

<img src="MiniX5.gif" width="1000">


This week was really challenging for me. I found it really difficult to articulate any rules and i didn't have any vision of what i wanted to do. Eventually i just did _something_ and slowly started growing the program. 

The first rule i made was about ellipses changing colors. The rule is as follows:

1. Everytime the ellipse is yellow, then it must turn either green or red. If the ellipse is green or red, it must turn yellow.

With this rule, ellipses would change colors based on the previous color. By deciding that the ellipse must turn _either_ green or red when it's yellow, i introduce an element of randomness, even if it is just a 50/50. In this way, the program will never be exactly the same, because you never know if the circle will be red or green. 

Next, i needed to figure out how the ellipses should be drawn. I decided to draw them horisontally like in [10PRINT](https://10print.org/).  Therefore, the second rule i made is as follows:

2. The placement of the red circle must be equal to the placement of the yellow circle -1 (on the y-axis). The placement of the green circle must be equal to the placement of the yellow circle + 1 (on the y-axis). 


The structure of the program is the following:

- The program contains two sequences; one that draws colored ellipses, and one which draws black rectangles (to give the illusion that the circles gets deleted). 
- Everytime the ellipses has been drawn all the way to the right side of the screen, it checks to see if they are placed in the bottom right corner. If not, it draws another line of circles. If it is, it initiates a new sequence which will be whatever of the two available sequences that is different from the one it just ran. 

This means that the resulting program draws a pattern of yellow, green and red circles (which is different every time) until it has filled the entire screen, whereas the circles gradually will be deleted. When all the circles are deleted, it will start the circle pattern again and continue repeating itself over and over. 

As mentioned above, i found this topic really challenging. The concept of an auto generator seems like it could potentially be a really large topic, and it can get quite complicated when it moves into areas of machine learning etc. I don't really think my project has anything interesting to contribute to this, but nevertheless, the process of creating it made me think a lot about the topic. There seems to be a lot of questions about the autonomy of both machine and human and the collaboration they partake in. Because the topic is so large, i want to just focus on the topic of autonomy for a moment. There is this question of whether the machine is an active agent in generative art or not, and where the creativity lies; is it with the human or the machine? Upon reflecting on this, i think that we often tend to give the computer too much power. Personally i don't think the computer is anything other than a tool, but we still have this idea that it is more than a tool, but less than a living being. But i would argue that every element we see in a computer and that we attribute to some sort of "more than a tool"-ness, comes from humans. The things that humans have made, coded, we give some of the credit of that to the computers, when in reality it only ecexutes commands. That is no different than using a regular old calculator or what happens when i press the start button on a blender. The functions in a computer is just infinitely more complicated. Therefore the computer can't really be an active agent in a generative program. Of course, if one would argue that all tools are the ones doing the job rather than the human, then that of course would be the same for a computer. But if you are of the opinion that it is you who are hammering the nail in the wall and not the hammer itself, then it is also the human that has sole agency in generative computer programs. 

This could of course be proven wrong with a thourough argument, but i haven't really been convinced of any i have seen yet. I although think it's a really interesting discussion in regards to this weeks topic, especially because i found it rather difficult to understand and articulate anything about it. I do however think that i have a better understanding of auto generative software now, which is probably influenced a lot by the argument i made above. I don't think it makes sense to talk about whether i am right or wrong in that argument, but i think that it was an interesting place to start and i believe that my view on this topic will definetly evolve further over time, which will be really fun to follow.
