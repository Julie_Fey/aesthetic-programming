let state = 1; // for green make state 1 // for red make state 2 // for yellow make state 3
let greenORred // decides wheter to make the ellipse green or red
let stateBlack = 0; // causes a "blackout" (wipes the screen)
let gridSize = 12.5;
let circleSize = 10;
let circleMoveX = 1; // where the ellipse is on the x-axis
let circleMoveY = 2; // where the ellipse is on the y-axis
let stopX = 1490; // decides when to move down to the next line
let stopY = 700; // decides when to stop the sequence and start over as either black or colored

function setup () {
  createCanvas(windowWidth, windowHeight);
  background(0);
  grid();
}

function draw() {
moveControl();
colorControl();
}

function moveControl() {
//checks if the circles have reached the bottom right corner - if yes, chech if need to blackout or make color, if no then continue to make another line of circles
  if (12.5*(circleMoveX+1)>stopX){
    checkWhenToBlackout();
  } else {
    circleMoveX++;
  }
}

function colorControl () {
  //decides wheter to make the circles black or colored when starting a new sequence
  if (stateBlack === 1 ) {
    blackout();
  } else {
    colorBomb();
  }
}

function colorBomb() {
  //color rule - if the circle is yellow, it should become either green or red next. If it is green or red, it should always become yellow
  if(state === 3) {
    greenORred = random(2);
    if (greenORred < 1) {
      greenCircle();
    } else {
      redCircle();
    }
  } else {
    yellowCircle();
  }
}

function blackout() {
  //makes it seem like the circles get deleted by replacing them with black squares if called
  fill("black");
  noStroke();
  rectMode(CENTER);
    rect(12.5*circleMoveX, 12.5*(circleMoveY + 1), circleSize);
    rect(12.5*circleMoveX, 12.5*(circleMoveY - 1), circleSize);
    rect(12.5*circleMoveX, 12.5*(circleMoveY), circleSize);
}

function checkWhenToBlackout() {
  //chanes between the colored circles and blackout. When it reaches the bottom right corner, if it is black, it should be made colored.
  // if it is colored, it should be made black.
  if (12.5*(circleMoveY+1)>stopY){
    if (stateBlack === 1) {
      stateBlack = 0;
    } else {
      stateBlack = 1;
    }
    // resets the coordinates to the top left corner
    circleMoveX = 1;
    circleMoveY = 2;
  } else {
  circleMoveX = 2;
  circleMoveY = circleMoveY + 3;
  }
}

function greenCircle() {
  fill("#94F06C");
    ellipse(12.5*circleMoveX, 12.5*(circleMoveY + 1), circleSize);
    state = 1;
}

function redCircle() {
  fill("#EE7777");
    ellipse(12.5*circleMoveX, 12.5*(circleMoveY - 1), circleSize);
    state = 2;
}

function yellowCircle() {
  fill("#F0E86C");
    ellipse(12.5*circleMoveX, 12.5*circleMoveY, circleSize);
    state = 3;

}

function grid(){
//draws a grid
  for (let x = 0; x <= width; x = x + gridSize) {
    for (let y = 0; y <= height; y = y + gridSize){
      stroke("white");
      strokeWeight(0.5);
      noFill();
      rectMode(CENTER);
      rect(x,y,gridSize);
    }
  }
}
