let time;
let light;
let youSay;
let iSee;
let iFeel;

function preload() {
  time = loadJSON("time.json");
  light = loadJSON("light.json");
  youSay = loadJSON("youSay.json");
  iSee = loadJSON("iSee.json");
  iFeel = loadJSON("iFeel.json");
  myFont = loadFont('vSHandprinted.otf');
}

function setup() {
createCanvas(windowWidth, windowHeight);
frameRate(1);
}

function draw() {
  background("black");
  fill("white");
  constantText();
  randomText();
}

function constantText() {
  textFont(myFont);
  textAlign(LEFT);
  textSize(30);
  text("it is", width/12, height/4);
  text("outside, it is", width/12, height/4+50);
  text("you say,", width/12, height/4+100);
  text("i see", width/12, height/4+150);
  text("i feel", width/12, height/4+200);
}

function randomText() {
  textFont(myFont);
  textAlign(LEFT);
  textSize(30);
  text(random(time.timeStamps), width/12+120, height/4);
  text(random(light.lights), width/12+290, height/4+50);
  text(random(youSay.phrases), width/12+170, height/4+100);
  text(random(iSee.something), width/12+120, height/4+150);
  text(random(iFeel.moods), width/12+135, height/4+200);
}
