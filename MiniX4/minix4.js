let state = true;
let img;
let img2;
let button;
let con;
let eth;

function preload() {
  img = loadImage("dna.jpg");
  img2 = loadImage("dna2.jpg");

}

function setup() {
  createCanvas(windowWidth, windowHeight);

  //gender radio
  radioG = createRadio();
  radioG.position(width/2 - 115, 400);
  radioG.option("Male");
  radioG.option("Female");
  radioG.option("Not specified");
  radioG.style("width/2", "60px");

  // ethnicity
  eth = createInput("");
  eth.size(100);
  eth.position(width/2 - 55, 455);

  // contact
  con = createInput("");
  con.size(100);
  con.position(width/2 - 55, 520);

  //checkbox Age
  yes = createCheckbox("Yes", false);
  no = createCheckbox("No", false);
  yes.position(width/2 - 50, 590);
  no.position(width/2 + 10, 590);

  //submit
  button = createButton("Submit");
  button.position(width/2 - 35, 640);
  button.mousePressed(buttonPressed);

}


function buttonPressed() {
  if(state === true) {
    state = false;
  } else {
    state = true;
  }
}


function draw() {
  if (state === true) {
    background("black");
    image(img, 0, 40, width/2, 200);
    image(img2, width/2, 40, width/2, 200);
    writings();
    survey();
  } else {
    submitted();
  }
}


function writings () {
  textSize(45);
  textAlign(CENTER);
  fill("white");
  stroke("#76B0D7");
  text("[̲̅A][̲̅n][̲̅o][̲̅n][̲̅y][̲̅m][̲̅o][̲̅u][̲̅s]", width/2, 60);
  noStroke();
  textSize(16);
  text("At Anonymous, we have everything you need to ensure your anonymity. Want to be sure not to leave a trace of DNA behind? No problem!", width/2, 250);
  text("Simply use our high quality DNA samples to disrupt and replace your own DNA. Spray it on your glass, cigarette bud or elsewhere, and discover the power of anonymity!", width/2, 270);
  text(" With our samples it will be impossible to identify you from the mixed DNA. Simply fill in the form to get started.", width/2, 290);
}


function survey() {
  rectMode(CENTER);
  fill("#76B0D7");
  noStroke();
  rect(width/2, 350 + 150, 300, 350);

  rectMode(CENTER);
  noFill();
  stroke(255);
  strokeWeight(1);
  rect(width/2, 350 + 150, 320, 370);

  //gender
  noStroke();
  textSize(16);
  fill("black");
  text("I'm looking for...", width/2, 350);
  text("Gender:" , width/2, 395);

  //ethnicity
  text("Ethnicity:", width/2, 450);

  //contact
  text("Contact information:", width/2, 510);

  //18+
  text("I am 18+", width/2, 573);

}


function submitted() {
  background("black")
  image(img, 0, 40, width/2, 200);
  image(img2, width/2, 40, width/2, 200);
  radioG.hide();
  button.hide();
  yes.hide();
  no.hide();
  eth.hide();
  con.hide();

  textSize(45);
  textAlign(CENTER);
  fill("white");
  stroke("#76B0D7");
  text("[̲̅A][̲̅n][̲̅o][̲̅n][̲̅y][̲̅m][̲̅o][̲̅u][̲̅s]", width/2, 60);

  textSize(32);
  noStroke();
  fill("white");
  text("We have received your request. We will be in touch.", width/2, height/2);
}
