# **[̲̅A][̲̅n][̲̅o][̲̅n][̲̅y][̲̅m][̲̅o][̲̅u][̲̅s]** #


[Click here to view ANONYMOUS](https://julie_fey.gitlab.io/aesthetic-programming/MiniX4/)

[The code](https://gitlab.com/Julie_Fey/aesthetic-programming/-/blob/master/MiniX4/index.html)


<img src="MiniX4.gif" width="1000">

## **Welcome to [̲̅A][̲̅n][̲̅o][̲̅n][̲̅y][̲̅m][̲̅o][̲̅u][̲̅s]** ##
#### *ANONYMOUS is a company that helps to ensure your anonymity! We help people all over the world stay anonymous by supplying high quality DNA samples to be used to earase or hide your own identity. Buy one today!* ####



### What is my program about, and the conceptual ideas behind it ###

"Capture all" - to me this does not imply anything good. To me, it sounds like surveillance and an invasion of privacy. Capturing data can also be used to improve products and ultimately lives, but the lack of transparency in the digital data field is alarming. I believe that transparency is one of the most important things, and everyone deserves to have control over and knowledge about their own data. 

For these and many other reasons, I am very interested in what Shoshana calls "surveillance capitalism", and so for this project I wanted to explore some aspect of this term. I find surveillance capitalism to be hugely problematic, innapropriate and, quite frankly, frightening, and so i wanted to somehow convey these feelings to others. I wanted to create something that was scary and uncomfortable, and makes people think a bit more about privacy and that people should care about their privacy and where their dato goes, instead of arguing that "i dont have anything to hide". 

The inspiration for this program came from Heather Dewey-Hagborg's work called ['Invisible'](https://deweyhagborg.com/projects/invisible). I was searching for inspiration, and this gave me the idea of thinking about DNA as data, instead of being limited to the kind of data that can be collected digitally. The result is this mock-up website/company, who sells other peoples DNA to whoever wants it. The customer can then use the DNA for whatever they want, sort of like how Google or Facebook sells our data to third parties, which then uses it exactly as they want, without any moral regard. 

**The company is supposed to work in the following way; you, the customer, fills out the form on the page, after which you will be contacted to talk further about your request. When you have established what kind of DNA you want (gender, ethnicity, whatever else DNA contains that helps identify people), you will buy the anonymous DNA sample that the company supplies. The samples all come from the "waste" we give away all the time; this could be cigarette buds, used gum, any kind of trash that could contain DNA and maybe even through deals with restuarants etc. that will supply the company with used glasses, cutlery or even half eaten food. This would mean, that simply through existing in the world and participating in society, you involentarily (and unknowingly) become a part of a DNA database that lets just about anyone buy your DNA and use it for whatever they please, such as faking maternity test, hiding their own DNA by mixing it with others or even using it to plant false evidence at crime scenes. The company won't reveal the "doner's" identity, and it is possible that you will never know that your data is being used, just like with the digital data we supply Google and Facebook with. I think most people would still prefer not to have their DNA sold. Why are most people fine with having other kinds of data sold then?**

The reason i choose to make this particular program, is to bring attention to the importance of privacy. Whenever i have a conversation with someone (note: someone who doesn't study anything IT related) about data and online privacy, they almost always say that they don't care if anyone uses their data, they have boring lives and they don't have anything to hide. I believe that you should care. Our data is, among other things, contributing to the horrible mass murders and imprisonment of almost 1,5 million Uyghur muslims in China. The question is not if you are okay with these big companies taking your data and using it for better marketing and surveillance or not, the question we should be asking ourselves, is if we are okay with our data contributing to the facial recognizing softwares that are used to find and watch Uyghur muslims in China, which ultimately leads to them being held captive in concentration camps and murdered. If a company took another type of data, like DNA, and sold it to whoever, we would not be okay with it, because that could have direct negative consequenses for ourselves. But we are okay with it if it only has negative consequenses for others? I believe that projects like this could help shift the focus and make people realize, that just because you can't see the damage, doesn't mean it doesn't exist. The data these companies take from us can be just as harmful as if they took our DNA. 

Quick notes on my code:
For this project i was mostly focused on the meaning behind the program, not so much to use complicated syntax. Therefore, the code is relatively simple. I used a couple of DOM elements, like the radio button and checkboxes, which was new, but i quickly figured out how to make it work. I tried to focus on the aestethic and vibe of the program, even though this is also relativly simple. I wish that i knew how to customize it a bit better and how to make it cleaner and more professional looking, but for now this was all i was able to do for the aesthetic. 
