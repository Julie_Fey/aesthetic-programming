# Aesthetic Programming #
Hello! This is my folder for the Aesthetic Programming Course on Aarhus University for the Spring semester 2021.
Before this course, I had almost zero programming experience. Click through to see my progress for each week!

Let's create some cool things!

<img src="happyBird.gif" width="500">
