# **MiniX9** #

[link to the program](https://julie_fey.gitlab.io/aesthetic-programming/MiniX9/)

[the code](https://gitlab.com/Julie_Fey/aesthetic-programming/-/blob/master/MiniX9/minix9.js)

### **Technical run-through** ###

For this minix we unfortunately didn’t have that much time, and so we decided to slightly modify the source code that we ran through during class – the example with using googles api for an image search.  

We decided to modify the query and import a json file containing many different nouns. We then asked the api to choose a random element from the json file for the search query. The result is that you get a random picture each time you load the page.  

Or at least that was the intention. Upon testing it multiple times, we discovered that around a third +- of the queries returned an error message in the console log. Since the code was the same either way, we figured that the error must be on the side of the api/the data. We tried printing the results in the console log, to see where in the returned json file the issue was, and we found that in the cases where it returned an error message, it was because of this line in the code;  

	getImg = data.items[0].image.thumbnailLink; 

In these cases “items” and thus “image” and “thumbnail” did not exist in the file – in fact we couldn’t find an image in any of the data we recieved. We decided to keep this element in the final program, because we think that it was quite interesting that we were only allowed acces to certain data. Of the words that did succes in bringing forth an image was words such as “rainbow” or “maltese”, whereas we instead got the error message with equally common words such as “pencil” and “terrier”.  

<img src="minix9.PNG" width="600">

 

### **Reflection** ###

In our minix project we chose to keep our error message in our program to illustrate the power and control big search engines like google have on the people using its systems and API. It is important to understand, identify and locate errors and bugs because they sometimes produce unexpexted results. Google is a very powerful corporation and even though they give access to some data, it is still manipulated and full of constraint. As we learned in class it is limited to 100 free API requests for all units from business to non-profit organizations. We also noticed how the images we did manage to pull from the api all were obscure versions of the key words. For example, when searching for “dog”, an image of a dark haired woman showed up. Therefore it seems that google only allows us to use its least popular search results, and as we discovered, in some cases no results at all.  

<img src="minix92.PNG" width="1000">


<img src="minix93.PNG" width="1000">

Google is a great contributer to targeted marketing and user profiling which raises questions about openness and transparancy when it comes to what we see and what is hiden from us. The algorithm is build on generalization and classifications that it is often portrayed as “the truth”. This structure of data collecting and representation is problematic and leave room for social, political and cultural implications.   

“Search happens in a highly commercial environment, and a variety of processes shape what can be found; these results are then normalized as believable and often presented as factual and become such a normative part of our experience with digital technology and computers that they socialize us into believing that these artifacts must therefore also provide access to credible, accurate information that is depoliticized and neutral.” (Soon, s. 206) 

API’s or application programming interfaces are what links the various platforms, applications and information processes together and makes it possible to collaborate between different software platforms. At the same time API’s also creates certain boundaries and control to what resources and information that are available and apparent to the user. Therfore API’s have enormous power when creating certain images and cultural and social norms.  

It would be interesting to further explore how and why google filters the data available throught the api so heavily. How do they choose the data that is okay for us to use through the api? It could be really interesting to explore further.  


### **References** ###

Soon, Winnie, and Geoff Cox. "Aesthetic programming: A handbook of software studies." (2020) 
