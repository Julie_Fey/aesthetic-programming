

let url = "https://www.googleapis.com/customsearch/v1?";
let apikey = "AIzaSyAf_nwF0EzFeEUuD2-8DIRuEF5hC1ahIus"
let engineID = "af98ab0274db96246";
let words;  //search keywords
let searchType = "image";
let imgSize ="medium";
let request;
let getImg;
let img;
let frameBorder = 60;


function preload() {
  words = loadJSON("words.json");
}


function setup() {
    createCanvas(windowWidth,windowHeight);
    background(255);
	  fetchImage();
}

function fetchImage() {

	request = url + "key=" + apikey + "&cx=" + engineID + "&imgSize=" + imgSize  + "&searchType=" + searchType + "&q=" + random(words.nouns);
	console.log(request);
	loadJSON(request, gotData);

}

function gotData(data) {
	getImg = data.items[0].image.thumbnailLink;
	console.log(getImg);
  img =createImg(getImg, "image", "", success);
  img.hide();
}

function success() {
  console.log("success");
  translate(width/2-img.width/2-frameBorder, height/2-img.height/2-frameBorder);
  noStroke();
  fill(239);
  image(img,0+frameBorder,0+frameBorder);
}
